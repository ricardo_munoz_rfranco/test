import groovy.transform.Field

@Field def lastStatus = null

def warnIfStatusChange(current) {
  if (lastStatus == null) {
    lastStatus = current
    return
  }

  if (lastStatus == current) return
  if (current == null) return
  echo '*********************************************************'
  echo "WARNING: Status changed from ${lastStatus} to ${current}."
  echo '*********************************************************'
  lastStatus = current
}

pipeline {
  parameters {
    string(name: 'node_label', defaultValue: 'docker-builder', description: 'Node label where execute build')
  }

  agent {
    node {
      label params.node_label
    }
  }

  options {
    disableConcurrentBuilds()
    timeout(time: 180, unit:'MINUTES')
    timestamps()
    buildDiscarder(logRotator(numToKeepStr: '10'))
    ansiColor('xterm')
  }

  environment {
    PROJECT_NAME = 'test'
  }

  stages {
    stage('Setup') {
      steps {
        script { if (currentBuild.result == null) { currentBuild.result = 'SUCCESS' } }
        script{warnIfStatusChange(currentBuild.result)}

        echo "Calculate the version number"
        script {
          def props = readProperties file: 'version.properties'
          def release_branch = ("${env.BRANCH_NAME}" ==~ /^Releases\/[0-9]+\.[0-9]+\.[0-9]+$/)
          if (release_branch) {
            env.VERSION="${env.BRANCH_NAME.replace('Releases/', '')}.${env.BUILD_NUMBER}"
          } else {
            env.VERSION="${props['version_major']}.${props['version_minor']}.${props['version_release']}.${env.BUILD_NUMBER}"
          }
        }
        sh '''#!/bin/bash
            ls -la
        '''
      }
    }
  }

  post {
    always {
      script {
         bitbucketStatusNotify(
          buildState: (currentBuild.result == 'FAILURE' ? 'FAILED' : 'SUCCESSFUL'),
        )
      }
    }
  }
}


